<?php if (!defined('defSimpleSite')) {die("Use site core!");}  ?>
<div align="center" id="main">
	<?php echo($page->item->content);?>
	<br />
	<img id="image" src="<?php echo($page->item->pic);?>" style="border:1px solid grey;padding:2px;">
	<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
	<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="icon" data-yashareQuickServices="vkontakte,facebook,twitter,gplus"></div>
	<div id="navigator">
		<?php $prevId = $page->item->getPrevId(); ?>
		<?php $nextId = $page->item->getNextId(); ?>
		<?php echo ($prevId)? "<a href='?id=$prevId'><<</a>":""; ?>
		<a href="?id=<?php echo($page->item->id); ?>">#</a>
		<?php echo ($nextId)? "<a href='?id=$nextId'>>></a>":""; ?>
	</div>
</div>