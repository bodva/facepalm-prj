<?php
if (!defined('defSimpleSite')) {die("Use site core!");}

class today_page extends page {
	var $item;
	var $itemnum;
	function today_page() {
		$this->item = new entry();
		$id = $this->getGet();
		if ($id === false) {
			$this->setLastItem();
		} else {
			if ($this->item->isexist($id)) {
				$this->setIdItem($id);
			} else {
				$this->setLastItem();
			}
		}
		$this->title = $this->item->title." | FACEPALM";
	}
	function getGet(){
		if (!empty($_REQUEST['id'])) {
			return (int)$_REQUEST['id'];
		}
		return false;
	}
	function setLastItem(){
		$this->itemnum = $this->item->getLastId();
		$this->item->load($this->itemnum);
	}
	function setIdItem($id){
		$this->itemnum = $id;
		$this->item->load($this->itemnum);

	}
} 

?>