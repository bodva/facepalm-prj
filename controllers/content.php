<?php if (!defined('defSimpleSite')) {die("Use site core!");}  ?>

<?php 
$page = new page();
$requestedpage = core::getfullnoquestion();
$controller_filename = './controllers/'.$requestedpage.'.php';
if (file_exists($controller_filename)) {
	include $controller_filename;
	$classname =$requestedpage.'_page';
	$page = new $classname(); 
}
include './template/_parts/_header.php';

$template_filename = './template/'.$requestedpage.'.php';
if (file_exists($template_filename)) {	
	include $template_filename;
} else {
	if ((!empty($_REQUEST['act']))&&($_REQUEST['act']== 'admin')) {
		include './admin/index.php';
	} else {
		include './template/index.php';
	}
}

include './template/_parts/_footer.php';
?>