<?php if (!defined('defSimpleSite')) {die("Use site core!");}  ?>
<?php
class entry {
	var $id;
	var $title;
	var $content;
	var $pic;

	static function save($saveid = -1){
		// die($saveid);
		core::debug("save");
		$savepc = new entry;
		if ($saveid != -1) { $savepc->load($saveid);}

		if (!empty($_REQUEST['title'])) {
			// $savepc->title = htmlspecialchars($_REQUEST['title']);
			$savepc->title = core::getrequest('title')->request;
		} else {
			$savepc->title = time();
			// $savepc->title = date("Y-m-d H:i:s:u");
		}
		// $savepc->content = htmlspecialchars($_REQUEST['content']);
		// $savepc->content = $_REQUEST['content'];
		$savepc->content = core::getrequest('content',false)->request;
		$pic =  core::getfile('pic',$savepc->title.'.jpg');
		if (!empty($pic)) {
			$savepc->pic = $pic;
		}
		// $savepc->pic = kconfig::$pic;
		
		if (core::$nicedie) {
			return $saveid;
		}
		if ($saveid != -1) {
			$savepc->update();
		} else {
			$savepc->add();
		}

		return $savepc->id;
	}

	static function admintable(){
		core::debug("admintable");
		$kd = new kdb;
		$sql = "SELECT 
						n0.eid,
						n0.title
					FROM `facepalm_entry` as n0
					ORDER BY n0.eid DESC";
		$kd->query($sql);
		$objs = array();
		while ($u0 = $kd->read()){
			$e = array();
			$e['id'] = $u0[0];
			$e['title'] = $u0[1];
			$objs[] = $e;
		}
		unset($kd);
		return $objs;
	}


	function getLastId() {
		$kd = new kdb;
		$sql = "SELECT MAX(`eid`) FROM `facepalm_entry`";
		$kd->query($sql);
		if ($u0 = $kd->read()) {
			return $u0[0];
		}
		return false; 	
	}

	function getPrevId() {
		$kd = new kdb;
		$sql = "SELECT `eid`
			FROM `facepalm_entry`
			WHERE `eid` < $this->id 
			ORDER BY`eid` DESC 
			LIMIT 1";
		$kd->query($sql);
		if ($u0 = $kd->read()) {
			return $u0[0];
		}
		return false;
	}

	function getNextId(){
		$kd = new kdb;
		$sql = "SELECT `eid`
			FROM `facepalm_entry`
			WHERE `eid` > $this->id 
			ORDER BY `eid` ASC 
			LIMIT 1";
		$kd->query($sql);
		if ($u0 = $kd->read()) {
			return $u0[0];
		}
		return false;
	}
	function adminentry($id=-1){
		// $entry = new entry;
		if ($id != -1 ) { $this->load($id);}
		$content = '';

		$this->title = (!empty($_REQUEST['title']))?($_REQUEST['title']):($this->title);
		$this->content = (!empty($_REQUEST['content']))?($_REQUEST['content']):($this->content);
		$this->pic = (!empty($_REQUEST['pic']))?($_REQUEST['pic']):($this->pic);
		
		$content.= 'ID:'.$this->id;
		$content.= '<br / >';
		$content.= '<input type="hidden" name="id" value="'.$this->id.'">';
		$content.= '<input type="text" name="title" value="'.$this->title.'">';
		$content.= '<br / >';
		$content.= '<textarea name="content" rows=8 cols=80>'.$this->content.'</textarea>';
		$content.= '<br / >';
		if (!empty($this->pic)) {
			$content .= "<img src=".$this->pic."><br/>";
		}
		$content.= '<input type="file" name="pic" value="'.$this->pic.'">';
		$content.= '<br / >';
		$content.= '<input type="submit" name="save">';

		return $content;
	}

	function add () {
		$kd = new kdb;
		$kd->query("INSERT INTO `facepalm_entry` (
			`eid`,
			`title`,
			`content`,
			`pic`
		) VALUES (
			NULL,
			'$this->title',
			'$this->content',
			'$this->pic'
		)
			");
		$this->id = $kd->getlastkey();
		unset($kd);

	}

	function update () {
		$kd = new kdb;
		$kd->query("UPDATE `facepalm_entry`
		SET
			`title` = '$this->title',
			`content` = '$this->content',
			`pic` = '$this->pic'
		WHERE
			`eid` = '$this->id'
		");
		unset($kd);
	}

	static function isexist($id){
		$result = false;
		$kd = new kdb;
		$kd->query("SELECT `eid` FROM `facepalm_entry` WHERE `eid`='$id'");
		$arr = $kd->read();
		$result = !empty($arr);
		unset($kd);
		return $result;
	}

	function load ($id) {
		$kd = new kdb;
		$kd->query("SELECT 
			`eid`,
			`title`,
			`content`,
			`pic`
			FROM `facepalm_entry`
				WHERE `eid`='$id'");
		if ($u0 = $kd->read()){
			$this->id = $u0[0];
			$this->title = $u0[1];
			$this->content = $u0[2];
			$this->pic = $u0[3];
		}
	}

	static function getEntrys($col = -1, $page = -1){
		$kd = new kdb;
		$sql = "SELECT 
						n0.eid,
						n0.title,
						n0.content,
						n0.pic
					FROM `facepalm_entry` as n0
					ORDER BY n0.eid DESC";
		if (($page > 0)&&($col>0)) {
			$start = $col*($page-1);
			$end = $col*$page;
			$sql .= " LIMIT $start,$end";
		}
		$kd->query($sql);
		$objs = array();
		while ($u0 = $kd->read()){
			$entry = new entry;
			$entry->id = $u0[0];
			$entry->title = $u0[1];
			$entry->content = $u0[2];
			$entry->pic = $u0[3];
			$objs[] = $entry;
		}
		unset($kd);
		return $objs;

	}

	function delete($id){
		$kd = new kdb;
		$kd->query("DELETE FROM `facepalm_entry` WHERE `eid`='$id'");
		unset($kd);
	}
}
?>